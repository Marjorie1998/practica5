package com.santanagilbert.practica5c;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogLogin = new Dialog(MainActivity.this);
                dialogLogin.setContentView(R.layout.dlg_log);

                Button btnau = (Button) dialogLogin.findViewById(R.id.btnau);
                final EditText edtc = (EditText) dialogLogin.findViewById(R.id.edtc);
                final EditText edtu = (EditText) dialogLogin.findViewById(R.id.edtu);

                btnau.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this, edtu.getText().toString() +
                                "" + edtc.getText().toString(),Toast.LENGTH_LONG);
                    }
                });
                dialogLogin.show();
                break;
            case R.id.opcionRegistrar:
                //Dialog dialogRegistrar = new Dialog(MainActivity.this);
                //dialogRegistrar.setContentView(R.layout.dlg_log);
                //dialogRegistrar.show();
                break;
        }
        return true;
    }
}


